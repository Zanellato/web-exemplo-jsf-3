package inscricao.cdi.beans;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Wilson
 */
@Named
@RequestScoped
public class IdiomasBean {

    @Inject
    private IdiomasListBean idiomasListBean;
    
    private Integer codigo;
    private String descricao;

    public IdiomasBean() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public void validaCodigo(FacesContext fc, UIComponent uic, Object codigo) {
        if (idiomasListBean.existeCodigo((Integer)codigo)) {
            throw new ValidatorException(
                new FacesMessage("Idioma " + codigo + " já existe"));
        }
    }
    
    public void validaDescricao(FacesContext fc, UIComponent uic, Object descricao) {
        if (idiomasListBean.existeDescricao((String)descricao)) {
            throw new ValidatorException(
                new FacesMessage("Idioma " + descricao + " já existe"));
        }
    }
    
    public void incluirIdiomaAction() {
        idiomasListBean.addIdioma(codigo, descricao);
    }
}
