/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Wilson
 */
public class Idioma implements Serializable {
    private Integer codigo;
    private String descricao;

    public Idioma() {
    }

    public Idioma(Integer codigo) {
        this.codigo = codigo;
    }

    public Idioma(Integer codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Idioma other = (Idioma) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }

}
